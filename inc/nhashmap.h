#ifndef NHASHMAP_H
#define NHASHMAP_H

#include <stdint.h>

#define NHASHMAP_RET_OK 0
#define NHASHMAP_RET_ERR -1

typedef struct nhashmap {
    int32_t *map;
    int32_t size;
    int32_t (*hash)(struct nhashmap*, int32_t);
} nhashmap;

extern int nhashmap_init_default(nhashmap *hashmap, int32_t size);
extern int nhashmap_init(nhashmap *hashmap, int32_t size,
        int32_t (*hash)(nhashmap*, int32_t));
extern int nhashmap_put(nhashmap *hashmap, int32_t key, int32_t val);
extern int nhashmap_get(nhashmap *hashmap, int32_t key, int32_t *val);
extern int nhashmap_destroy(nhashmap *hashmap);

#endif
