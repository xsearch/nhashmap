#!/bin/bash

SEED=11155
THREADS=( 1 2 4 )
SIZE=30
NUMELEMS=29

for t in "${THREADS[@]}"
do
    ./bin/benchmark1 $SEED $SIZE $NUMELEMS $t
done

