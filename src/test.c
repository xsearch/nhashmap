#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "nhashmap.h"

#define TOTAL_TESTS 2
#define SEED 11155
#define SIZE 1 << 30
#define NUMVALS 1 << 20

void init_vals(int32_t vals[NUMVALS])
{
    int i;
    
    srand(SEED);
    for (i = 0; i < NUMVALS; i++) {
        vals[i] = rand() % (SIZE / 2);
    }
}

int test_put_vals(nhashmap *hashmap, int32_t vals[NUMVALS])
{
    int i, rc;

    for (i = 0; i < NUMVALS; i++) {
        rc = nhashmap_put(hashmap, vals[i], vals[i]);
        if (rc != NHASHMAP_RET_OK) {
            printf("BUG: nhashmap_put returned %d, while expecting %d!\n",
                    rc, NHASHMAP_RET_OK);
            return 1;
        }
    }

    return 0;
}

int test_get_vals(nhashmap *hashmap, int32_t vals[NUMVALS])
{
    int i, rc, val;

    for (i = 0; i < NUMVALS; i++) {
        val = 0;
        rc = nhashmap_get(hashmap, vals[i], &val);
        if (rc != NHASHMAP_RET_OK) {
            printf("BUG: nhashmap_get returned %d, while expecting %d!\n",
                    rc, NHASHMAP_RET_OK);
            return 1;
        }
        if (val != vals[i]) {
            printf("BUG: nhashmap_get got %d, while expecting %d!\n",
                    val, vals[i]);
            return 1;
        }
    }

    return 0;
}

int test_default(const char teststr[])
{
    int rc;
    int32_t *vals;
    nhashmap hashmap;

    printf("%s\n", teststr);
    vals = (int32_t*) malloc(sizeof(int32_t) * NUMVALS);
    init_vals(vals);
    
    rc = nhashmap_init_default(&hashmap, SIZE); 
    if (rc != NHASHMAP_RET_OK) {
        printf("BUG: nhashmap_init returned %d, while expecting %d\n!",
                rc, NHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: nhashmap_init successful.\n");

    rc = test_put_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: nhashmap_put successful.\n");

    rc = test_get_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: nhashmap_get successful.\n");

    rc = nhashmap_destroy(&hashmap);
    if (rc != NHASHMAP_RET_OK) {
        printf("BUG: nhashmap_destroy returned %d, while expecting %d\n!",
                rc, NHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: nhashmap_destroy successful.\n");

    free(vals);
    printf("PASS: test_custom passed!\n");
    return 1;
}

int32_t test_hash(nhashmap *hashmap, int32_t val)
{
    return val % (hashmap->size / 2);
}

int test_custom(const char teststr[])
{
    int rc;
    int32_t *vals;
    nhashmap hashmap;

    printf("%s\n", teststr);
    vals = (int32_t*) malloc(sizeof(int32_t) * NUMVALS);
    init_vals(vals);
    
    rc = nhashmap_init(&hashmap, SIZE, test_hash); 
    if (rc != NHASHMAP_RET_OK) {
        printf("BUG: nhashmap_init returned %d, while expecting %d\n!",
                rc, NHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: nhashmap_init successful.\n");

    rc = test_put_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: nhashmap_put successful.\n");

    rc = test_get_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: nhashmap_get successful.\n");

    rc = nhashmap_destroy(&hashmap);
    if (rc != NHASHMAP_RET_OK) {
        printf("BUG: nhashmap_destroy returned %d, while expecting %d\n!",
                rc, NHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: nhashmap_destroy successful.\n");

    free(vals);
    printf("PASS: test_custom passed!\n");
    return 1;
}

int main(int argc, char *argv[argc])
{
    int num_tests = 0;

    printf("Running NHashMap tests ...\n");
    num_tests += test_default("Testing hashmap with default hash function.");
    num_tests += test_custom("Testing hashmap with custom hash function.");
    printf("Finished running tests. %d/%d tests have passed.\n", 
            num_tests, TOTAL_TESTS);

    return 0;
}
