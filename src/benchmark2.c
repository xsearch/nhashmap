#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <sched.h>
#include "nhashmap.h"

#define DEBUG 0
#define HELP "Required parameters: <seed> <size> <num elem> <num threads>\n"

#define MACHINE_THREADS 4
#define MACHINE_CORES 2
#define MACHINE_NUMA_NODES 1

typedef struct thread_arg {
    int tid;
    int32_t size;
    int32_t num_elems;
    int32_t *elems;
    nhashmap *hashmap;
    pthread_barrier_t *barrier;
    pthread_t *thread;
    int rc;
} thread_arg;

void *work(void *param)
{
    int i, rc;
    thread_arg *arg;
    nhashmap *hashmap;
    int32_t *elems;
    int32_t num_elems;
    cpu_set_t cpuset;

    arg = (thread_arg *) param;
    arg->rc = 0;
    num_elems = arg->num_elems;
    
    // set thread affinity
    CPU_ZERO(&cpuset);
    CPU_SET(arg->tid % MACHINE_THREADS, &cpuset);

    rc = pthread_setaffinity_np(*(arg->thread), sizeof(cpu_set_t), &cpuset);
    if (rc) {
        printf("ERROR: thread %d could not set affinity!\n", arg->tid);
        arg->rc = 3;
    }

    // allocate memory for hashmap and array of elements
    hashmap = (nhashmap*) malloc(sizeof(nhashmap));
    if (hashmap == NULL) {
        printf("ERROR: thread %d could not allocate hashmap!\n", arg->tid);
        arg->rc = 1;
    }

    rc = nhashmap_init_default(hashmap, arg->size);
    if (rc != NHASHMAP_RET_OK) {
        printf("ERROR: thread %d could not initialize hashmap!\n", arg->tid);
        arg->rc = 1;
    }
    arg->hashmap = hashmap;

    elems = (int32_t*) malloc(sizeof(int32_t) * num_elems);
    if (elems == NULL) { 
        printf("ERROR: thread %d could not allocate elements!\n", arg->tid);
        arg->rc = 1;
    }
    arg->elems = elems;
    // complete barrier in order to allow main thread to do initialization
    pthread_barrier_wait(arg->barrier);

    // wait for main thread to initialize the array of elements
    pthread_barrier_wait(arg->barrier);

    // put elements in the hashmap
    if (arg->rc) {
        pthread_exit(NULL);
    }

    for (i = 0; i < num_elems; i++) {
        rc = nhashmap_put(hashmap, elems[i], elems[i]);
        if (rc != NHASHMAP_RET_OK) {
            arg->rc = 2;
        }
    }

    pthread_exit(NULL);
}


void benchmark(int32_t seed, int32_t size, int32_t num_elems, int num_threads)
{
    int i, j, k, rc, rc_agg, val, val_agg;
    pthread_t threads[num_threads];
    pthread_barrier_t *barrier;
    thread_arg args[num_threads];
    struct timeval put_tik, put_tok, get_tik, get_tok;
    double put_time, get_time;
    
    // allocate barrier
    barrier = (pthread_barrier_t*) malloc(sizeof(pthread_barrier_t));
    if (barrier == NULL) {
        printf("ERROR: could not allocate barrier!\n");
        return;
    }

    // initialize barrier
    rc = pthread_barrier_init(barrier, NULL, num_threads + 1);
    if (rc) {
        printf("ERROR: could not initialize barrier!\n");
        return;
    }

    // initialize thread arguments
    if (DEBUG) {
        printf("INFO: initializing and creating threads\n");
    }
    for (i = 0; i < num_threads; i++) {
        args[i].tid = i;
        args[i].size = size / num_threads;
        args[i].num_elems = num_elems / num_threads;
        args[i].barrier = barrier;
        args[i].thread = &threads[i];
    }

    // create threads
    for (i = 0; i < num_threads; i++) {
        rc = pthread_create(&threads[i], NULL, work, &args[i]);
        if (rc) {
            printf("ERROR: could not create thread %d!\n", i);
        }
    }

    // wait for threads to allocate memory for the array of elements
    pthread_barrier_wait(barrier);

    // initialize the array of elements of each thread
    if (DEBUG) {
        printf("INFO: initializing the array of elements for each thread\n");
    }
    srand(seed);
    for (i = 0; i < num_threads; i++) {
        for (j = 0; j < (num_elems / num_threads); j++) {
            args[i].elems[j] = rand() % (size / (num_threads * 2)) + 1;
        }
    }

    if (DEBUG) {
        printf("INFO: started running put and get operations\n");
    }
    // complete barrier that allows the threads to continue execution
    pthread_barrier_wait(barrier);

    // start measuring time
    gettimeofday(&put_tik, NULL);

    // join threads while measuring time
    rc_agg = 0;
    for (i = 0; i < num_threads; i++) {
        rc = pthread_join(threads[i], NULL);
        if (rc) {
            printf("ERROR: could not join thread %d!\n", i);
        }
        if (args[i].rc) {
            rc_agg = 1;
        }
    }

    // stop measuring time
    gettimeofday(&put_tok, NULL);
    
    // calculate and print put throughput
    put_time = ((double) put_tok.tv_sec - (double) put_tik.tv_sec) * 1000000 
            + ((double) put_tok.tv_usec - (double) put_tik.tv_usec);
    printf("RESULTS: put throughput = %lf OPS\n",
            (double) num_elems / (put_time / 1000000));
    if (rc_agg) {
        printf("WARN: the hashmap put is buggy!\n");
    }

    // start measuting time
    gettimeofday(&get_tik, NULL);
    
    // Measure the time it takes to lookup all the elements
    rc_agg = 0;
    for (i = 0; i < num_threads; i++) {
        for (j = 0; j < (num_elems / num_threads); j++) {
            val_agg = 0;
            for (k = 0; k < num_threads; k++) {
                val = 0;
                rc = nhashmap_get(args[k].hashmap, args[i].elems[j], &val);
                if (rc != NHASHMAP_RET_OK) {
                    rc_agg = 1;
                }
                if (val != 0) {
                    val_agg = val;
                }
            }
            if (val_agg != args[i].elems[j]) {
                rc_agg = 1;
            }
        }
    }

    // stop measuring time
    gettimeofday(&get_tok, NULL);
    
    // calculate and print get throughput
    get_time = ((double) get_tok.tv_sec - (double) get_tik.tv_sec) * 1000000 
            + ((double) get_tok.tv_usec - (double) get_tik.tv_usec);
    printf("RESULTS: get throughput = %lf OPS\n",
            (double) num_elems / (get_time / 1000000));
    if (rc_agg) {
        printf("WARN: the hashmap get is buggy!\n");
    }
    
    // destroy barrier and free memory
    pthread_barrier_destroy(barrier);
    free(barrier);
    for (i = 0; i < num_threads; i++) {
        nhashmap_destroy(args[i].hashmap);
        free(args[i].hashmap);
        free(args[i].elems);
    }
}

int main(int argc, char *argv[argc])
{
    int num_threads;
    int32_t seed, size, num_elems;

    // check for right number of program arguments
    if (argc != 5) {
        printf(HELP);
        return 0;
    }

    // extract program arguments
    seed = (int32_t) atoi(argv[1]);
    size = (int32_t) atoi(argv[2]);
    num_elems = (int32_t) atoi(argv[3]);
    num_threads = atoi(argv[4]);

    // start the benchmark
    printf("Running the NHashMap benchmark #2 ...\n");
    benchmark(seed, 1 << size, 1 << num_elems, num_threads);
    printf("Finished running the benchmark.\n");

    return 0;
}
