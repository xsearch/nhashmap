#include <stdio.h>
#include <stdlib.h>
#include "nhashmap.h"

int32_t nhashmap_default_hash(nhashmap *hashmap, int32_t val)
{
    return val % hashmap->size;
}

int nhashmap_init_default(nhashmap *hashmap, int32_t size)
{
    if (size <= 0) {
        printf("ERROR: hashmap must have a size greater than 0!\n");
        return NHASHMAP_RET_ERR;
    }
    hashmap->size = size;

    hashmap->hash = nhashmap_default_hash;

    hashmap->map = (int32_t*) malloc(sizeof(int32_t) * size);
    if (hashmap->map == NULL) {
        printf("ERROR: could not allocated hashmap!\n");
        return NHASHMAP_RET_ERR;
    }

    return NHASHMAP_RET_OK;
}

int nhashmap_init(nhashmap *hashmap, int32_t size,
        int32_t (*hash)(nhashmap*, int32_t))
{
    if (size <= 0) {
        printf("ERROR: hashmap must have a size greater than 0!\n");
        return NHASHMAP_RET_ERR;
    }
    hashmap->size = size;

    hashmap->hash = hash;

    hashmap->map = (int32_t*) malloc(sizeof(int32_t) * size);
    if (hashmap->map == NULL) {
        printf("ERROR: could not allocated hashmap!\n");
        return NHASHMAP_RET_ERR;
    }

    return NHASHMAP_RET_OK;
}

int nhashmap_put(nhashmap *hashmap, int32_t key, int32_t val)
{
    int k;

    k = hashmap->hash(hashmap, key);
    hashmap->map[k] = val;

    return NHASHMAP_RET_OK;
}

int nhashmap_get(nhashmap *hashmap, int32_t key, int32_t *val)
{
    int k;

    k = hashmap->hash(hashmap, key);
    *val = hashmap->map[k];

    return NHASHMAP_RET_OK;
}

int nhashmap_destroy(nhashmap *hashmap)
{
    hashmap->size = -1;
    hashmap->hash = nhashmap_default_hash;
    free(hashmap->map);
    hashmap->map = 0;

    return NHASHMAP_RET_OK;
}
