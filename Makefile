CC=gcc
CFLAGS=-Wall
OFLAGS=-O2
IFLAGS=-Iinc
LIBS=-lpthread

test: bin src/test.c src/nhashmap.c
	$(CC) $(CFLAGS) $(OFLAGS) $(IFLAGS) -o bin/test src/test.c src/nhashmap.c 

benchmark1: bin src/benchmark1.c src/nhashmap.c
	$(CC) $(CFLAGS) $(OFLAGS) $(IFLAGS) -o bin/benchmark1 src/benchmark1.c src/nhashmap.c $(LIBS)

benchmark2: bin src/benchmark2.c src/nhashmap.c
	$(CC) $(CFLAGS) $(OFLAGS) $(IFLAGS) -o bin/benchmark2 src/benchmark2.c src/nhashmap.c $(LIBS)

bin:
	mkdir -p bin

clean: bin
	rm -r bin/*
