# NHashMap
Project name: NHashMap  
Author: fenrir  

Description: NHashMap is a simple and naive implementation of a hashmap that has no collision
detection, it overwrites values of the same bucket and is NUMA-aware.